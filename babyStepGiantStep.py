#!/usr/bin/env python3
"""
    Computes discrete logarithm over given elliptic curve over given Z_p
    by Baby-step giant-step algorithm
"""

from math import ceil, sqrt

# given module p
p = pow(2, 40) + 27

# extended Euclidean algorithm
def egcd(b, a):
    x0, x1, y0, y1 = 1, 0, 0, 1
    while a != 0:
        q, b, a = b // a, a, b % a
        x0, x1 = x1, x0 - q * x1
        y0, y1 = y1, y0 - q * y1
    return b, x0, y0 # gcd, inversion of a given b in a field of module a, _

class ZFieldElement:
    """Class representing element from field Z_p"""

    # constructor
    def __init__(self, value, p = p):
        self.p = p
        self.value = (value % p)

    # repr function output
    def __repr__(self):
        return str(self.value)

    # print function output
    def __str__(self):
        return str(self.value)

    # operator ==
    def __eq__(self, other):
        return (self.p == other.p) and (self.value % self.p == other.value % other.p)

    # returns addition of two elements (self + other)
    def __add__(self, other):
        return ZFieldElement((self.value + other.value) % self.p)

    # returns subtraction of two elements (self - other)
    def __sub__(self, other):
        return ZFieldElement((self.value - other.value) % self.p)

    # returns inversion -(self)
    def inv(self):
        return ZFieldElement(-self.value % self.p)

    # returns multiplication of two elements (self * other)
    def __mul__(self, other):
        return ZFieldElement((self.value * other.value) % self.p)

    # returns power of two elements (self ** power)
    def __pow__(self, power, modulo=None):
        return ZFieldElement(pow(self.value, power.value, self.p))

    # returns multiplicative inversion of an element (self)^(-1)
    # x = mulinv(self) mod p, (x * self) % p == 1
    def multInv(self):
        g, x, _ = egcd(self.value, self.p)
        if g == 1:
            return ZFieldElement(x % self.p)

    # returns result after dividing an element by another element (self / other) = (self * other^(-1))
    def __truediv__(self, other):
        return ZFieldElement((self.value * other.multInv().value) % self.p)

class EC:
    """Class representing elliptic curve"""

    # constructor
    def __init__(self, a, b):
        self.a = a
        self.b = b


class ECPoint:
    """
        Class representing point on an elliptic curve E with coordinates x, y
        A true variable isPointAtInfinity says the point is "point at infinity"
    """

    # constructor
    def __init__(self, x, y, isPointAtInfinity = False):
        self.x = x
        self.y = y
        self.isPointAtInfinity = isPointAtInfinity

    # transform the point coordinates to a tuple (to enable its usage as an index in a hash table)
    def hashable(self):
        return (self.x.value, self.y.value)

    # print function output
    def __str__(self):
        return "x: " + repr(self.x) + " y: " + repr(self.y)

    # operator ==
    def __eq__(self, other):
        if self.isPointAtInfinity and other.isPointAtInfinity:
            return True
        elif self.isPointAtInfinity:
            return False
        elif other.isPointAtInfinity:
            return False
        else:
            return (self.x.p == self.y.p) and (self.x == other.x) and (self.y == other.y)

    # sets a curve the point is on to E
    def setCurve(self, E):
        self.E = E

    # function for sumation of two points on elliptic curve (self + Q)
    def add(self, Q):
        if self.isPointAtInfinity: # self = "point at infinity"
            return Q
        elif Q.isPointAtInfinity: # Q = "point at infinity"
            return self
        elif (self.x.value == Q.x.value) and (self.y.value == Q.y.inv().value): # P = -Q
            return ECPoint(ZFieldElement(0), ZFieldElement(0), True)
        else:
            if self == Q: # P = Q
                numerator = ZFieldElement(3) * (self.x ** ZFieldElement(2)) + a  #(3*pow(self.x, 2) + (a % p)) % p
                denominator = ZFieldElement(2) * self.y
                lam = numerator/denominator
            else: # P != Q
                lam = (Q.y - self.y) / (Q.x - self.x)

            newX = (lam**ZFieldElement(2)) - self.x - Q.x
            newY = lam * (self.x - newX) - self.y
            return ECPoint(newX, newY)

    # function for multiplication of a point by an integer n*self
    def mult(self, n):
        result = self
        for i in range(1, n):
            result = self.add(result)
        return result

    # iversion of a point on EC
    def inv(self):
        return ECPoint(self.x, self.y.inv())

# implementation of the Baby-step giant-step algorithm
def bsgs(P, Q, P_order):
    '''
        Solves for x in Q = x*P over an elliptic curve over Z_p.
    '''

    N = ceil(sqrt(P_order)) + 1  # depends on a source (used the one given in scripts for MI-MKY)

    # Baby step.
    # Store hashmap of P, 2P, ... NP
    tbl = {}
    prevResult = ECPoint(0, 0, True) # 0th element would be "point at infinity"
    for i in range(1, N):
        currResult = prevResult.add(P)
        hashCurrResult = currResult.hashable()
        tbl[hashCurrResult] = i
        prevResult = currResult

    # Giant step.
    # computes Q, Q+N(-P), Q+2N(-P), ..., Q+(N-1)N(-P)
    # and find the equivalence in the hashmap table

    # first element - Q
    prevResult = Q
    # precompute n*(-P)
    nP_inv = P.inv().mult(N)

    for j in range(1, N):
        currResult = prevResult.add(nP_inv)
        # Search for an equivalence in the table.
        if currResult.isPointAtInfinity == False and currResult.hashable() in tbl:
            return j * N + tbl[currResult.hashable()] # j(giant step)N + i (baby step) = x
        prevResult = currResult

    # if solution not found
    return None

# given problem
a = ZFieldElement(-1)
b = ZFieldElement(0)
E = EC(a, b)
P = ECPoint(ZFieldElement(3), ZFieldElement(2585046525))
P.setCurve(E)
P_order = 549755813902 # counted in Sage by P.order()
Q = ECPoint(ZFieldElement(566039643735), ZFieldElement(511036707747))
Q.setCurve(E)

# run the bsgs function and show the result
print("The discrete logarithm is ", bsgs(P, Q, P_order))
